#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

	double x1, y1, x2, y2, p;
	cout << setprecision(12);
	while (true) {
		cin >> x1;
		if (x1 == 0)
			break;
		cin >> y1 >> x2 >> y2 >> p;

		double r = pow(pow(abs(x2 - x1), p) + pow(abs(y2 - y1), p), 1 / p);

		cout << r << endl;

	}

	return 0;
}