#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n;
    cin >> n;

    for (int i = 0; i < n; ++i) {
        string name;
        cin >> name;
        int py, pm, pd, by, bm, bd, courses;
        char fake;
        cin >> py >> fake >> pm >> fake >> pd >> by >> fake >> bm >> fake >> bd >> courses;

        cout << name << " ";

        if (py >= 2010) {
            cout << "eligible" << endl;
        } else if (by >= 1991) {
            cout << "eligible" << endl;
        } else if (courses >= 41) {
            cout << "ineligible" << endl;
        } else {
            cout << "coach petitions" << endl;
        }
    }

    return 0;
}