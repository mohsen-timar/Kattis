#include <iostream>
#include <cmath>

using namespace std;

int main() {

	char board[7][7]{0};

	for (int i = 0; i < 7; ++i) {
		for (int j = 0; j < ((i < 2 || i > 4) ? 3 : 7); ++j) {
			cin >> board[i][j + ((i < 2 || i > 4) ? 2 : 0)];
		}
	}

	int validMoves = 0;

	for (int i = 0; i < 7; ++i) {
		for (int j = 0; j < 7; ++j) {
			if (i > 1) { //Top

				if (board[i][j] != 'o')
					continue;

				if (board[i - 1][j] == 'o' && board[i - 2][j] == '.')
					validMoves++;

			}

			if (i < 5) { //Down
				if (board[i][j] != 'o')
					continue;

				if (board[i + 1][j] == 'o' && board[i + 2][j] == '.')
					validMoves++;
			}

			if (j > 1) { //Left
				if (board[i][j] != 'o')
					continue;

				if (board[i][j - 1] == 'o' && board[i][j - 2] == '.')
					validMoves++;
			}

			if (j < 5) { //Right
				if (board[i][j] != 'o')
					continue;

				if (board[i][j + 1] == 'o' && board[i][j + 2] == '.')
					validMoves++;
			}
		}
	}

	cout << validMoves << endl;

	return 0;
}