#include <iostream>
#include <cmath>
#include <map>

using namespace std;

void split(string line, string &en, string &ot) {
	unsigned long p = line.find(' ');
	en = line.substr(0, p);
	ot = line.substr(p + 1);
}

int main() {

	string en, ot, line;
	map<string, string> dic;

	while (true) {
		getline(cin, line);
		if (line.empty())
			break;
		split(line, en, ot);
		dic[ot] = en;
	}

	while (cin >> line) {
		auto w = dic.find(line);
		if (w == dic.end()) {
			cout << "eh" << endl;
		} else {
			cout << w->second << endl;
		}
	}

	return 0;
}