#include <iostream>
#include <cmath>
#include <sstream>

using namespace std;

char *toBase(string f, string s) {
	char *base = new char[f.size()];
	for (int i = 0; i < f.size(); ++i) {
		for (int j = 0; j < s.size(); ++j) {
			if (f[i] == s[j]) {
				base[i] = (char) j;
			}
		}
	}
	return base;
}

unsigned long long baseToTen(char *num, int count, int baseMax) {
	unsigned long long result = 0;

	for (int i = 0; i < count; ++i) {
		result = result * baseMax + (int) num[i];
	}

	return result;
}

string tenToBase(unsigned long long ten, string t) {
	unsigned long base = t.size();
	int i = (int) (log(ten) / log(base));
	string result;
	while (i >= 0) {
		result.insert(result.begin(), t[ten % base]);
		ten = ten / base;
		i--;
	}
	return result;
}

string translate(string f, string s, string t) {
	int count;
	char *base = toBase(f, s);
	unsigned long long ten = baseToTen(base, f.size(), s.size());
	return tenToBase(ten, t);
}

int main() {

	string first, second, their;
	int n;
	cin >> n;
	for (int i = 0; i < n; ++i) {
		cin >> first >> second >> their;
		string translated = translate(first, second, their);
		cout << "CASE #" << (i + 1) << ": " << translated << endl;
	}

	return 0;
}