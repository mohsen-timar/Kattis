#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

    int n;
    string line;

    map<int, string> info;
    cin >> n;

    for (int i = 0; i < n; ++i) {

        cin >> line;
        int r;

        if (line[0] >= '0' && line[0] <= '9') {
            r = stoi(line) / 2;
            cin >> line;
            info[r] = line;
        } else {
            cin >> r;
            info[r] = line;
        }
    }

    for (auto m : info) {
        cout << m.second << endl;
    }

    return 0;
}