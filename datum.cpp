#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    int d, m;
    cin >> d >> m;

    time_t t1, t2;
    struct tm t{0};
    t.tm_mday = d;
    t.tm_year = 2009;
    t.tm_mon = m - 1;

    t2 = mktime(&t);

    t.tm_mday = 1;
    t.tm_mon = 0;

    t1 = mktime(&t);

    int diff = (int) (difftime(t2, t1) / (60 * 60 * 24));
    diff += 3;
    diff %= 7;

    vector<string> day({"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"});

    cout << day.at(diff) << endl;

    return 0;
}