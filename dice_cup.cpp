#include <iostream>

using namespace std;

int main() {

    int n, m;
    cin >> n >> m;

    int x[n + m + 1]{0};

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            x[i + j]++;
        }
    }

    int max = 0;

    for (int i = 1; i <= n + m; ++i) {
        if (x[i] > max)
            max = x[i];
    }

    for (int i = 1; i <= n + m; ++i) {
        if (x[i] == max) {
            cout << i << endl;
        }
    }

    return 0;
}