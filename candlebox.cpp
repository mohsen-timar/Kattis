#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int D, R, T;
	cin >> D >> R >> T;

	int a = 0, b = 0;
	for (int i = 4;; ++i) {
		a += i;
		if (i - D >= 3) {
			b += i - D;
		}
		if (a + b == R + T) {
			break;
		}
	}

	cout << (R - a) << endl;

	return 0;
}