#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    string line;
    vector<string> lines;
    unsigned long maxLineLen = 0;

    while (getline(cin, line)) {
        if (line.length() > maxLineLen)
            maxLineLen = line.length();
        lines.push_back(line);
    }

    unsigned long long p = 0;

    for (int i = 0; i < lines.size() - 1; ++i) {
        p += pow(maxLineLen - lines.at(i).length(), 2);
    }

    cout << p << endl;

    return 0;
}