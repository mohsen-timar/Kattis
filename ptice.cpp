#include <iostream>
#include <cmath>

using namespace std;

int max(initializer_list<int> l) {
    int m = -1;
    for (auto k : l) {
        if (k > m)
            m = k;
    }
    return m;
}

int main() {

    int n;
    cin >> n;

    string line;
    cin >> line;

    string a = "ABC",
            b = "BABC",
            c = "CCAABB";

    int aa = 0, ba = 0, ca = 0;
    for (int i = 0; i < line.length(); ++i) {
        if (line[i] == a[i % a.length()])
            aa++;
        if (line[i] == b[i % b.length()])
            ba++;
        if (line[i] == c[i % c.length()])
            ca++;
    }

    int best = max({aa, ba, ca});

    cout << best << endl;

    if (aa == best) {
        cout << "Adrian" << endl;
    }

    if (ba == best) {
        cout << "Bruno" << endl;
    }

    if (ca == best) {
        cout << "Goran" << endl;
    }

    return 0;
}