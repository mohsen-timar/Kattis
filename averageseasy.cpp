#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    int n;
    cin >> n;

    for (int i = 0; i < n; ++i) {

        int m, t;
        cin >> m >> t;

        int count = 0;
        vector<int> cs;
        double csAvg = 0, eAvg = 0;

        for (int j = 0; j < m; ++j) {
            int k;
            cin >> k;
            cs.push_back(k);
            csAvg += k;
        }

        csAvg /= m;

        for (int j = 0; j < t; ++j) {
            int k;
            cin >> k;
            eAvg += k;
        }

        eAvg /= t;

        for (auto c : cs) {
            if (c < csAvg && c > eAvg) {
                count++;
            }
        }

        cout << count << endl;

    }


    return 0;
}