#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

    double D, V;
    cout << setprecision(12);
    while (true) {
        cin >> D >> V;
        if (D == 0 && V == 0) {
            break;
        }
        double res = pow((6 * ((M_PI * pow(D, 3) / 4) - V) / M_PI) - (pow(D, 3) / 2), 1.0 / 3);
        cout << res << endl;
    }
    return 0;
}