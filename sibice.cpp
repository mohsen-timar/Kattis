#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n, w, h;

	cin >> n >> w >> h;

	double l = sqrt(w * w + h * h);

	for (int i = 0; i < n; ++i) {
		int k;
		cin >> k ;
		cout << ((k <= l) ? "DA" : "NE") << endl;
	}

	return 0;
}