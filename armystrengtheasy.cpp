#include <iostream>
#include <cmath>

using namespace std;

int comp(const void *elem1, const void *elem2) {
	int f = *((int *) elem1);
	int s = *((int *) elem2);
	return f - s;
}

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {

		int ng, nm;
		cin >> ng >> nm;

		int g[ng], m[nm];

		for (int j = 0; j < ng; ++j) {
			cin >> g[j];
		}

		for (int j = 0; j < nm; ++j) {
			cin >> m[j];
		}

		qsort(g, sizeof(g) / sizeof(*g), sizeof(*g), comp);
		qsort(m, sizeof(m) / sizeof(*m), sizeof(*m), comp);

		int k = 0, l = 0;
		while (true) {
			if (g[k] >= m[l]) {
				l++;
			} else {
				k++;
			}

			if (l == nm) {
				cout << "Godzilla" << endl;
				break;
			}

			if (k == ng) {
				cout << "MechaGodzilla" << endl;
				break;
			}
		}

	}


	return 0;
}