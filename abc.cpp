#include <iostream>
#include <cmath>

using namespace std;

void sort(int *nums) {
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			if (nums[i] < nums[j]) {
				std::swap(nums[i], nums[j]);
			}
		}
	}
}

int main() {

	int nums[3];
	string order;
	cin >> nums[0] >> nums[1] >> nums[2];
	sort(nums);
	getline(cin, order);
	if (order.empty())
		getline(cin, order);

	for (int i = 0; i < 3; ++i) {
		if (i < 2) {
			if (order[i] == 'A') {
				cout << nums[0] << " ";
			} else if (order[i] == 'B') {
				cout << nums[1] << " ";
			} else if (order[i] == 'C') {
				cout << nums[2] << " ";
			}
		} else {
			if (order[i] == 'A') {
				cout << nums[0] << endl;
			} else if (order[i] == 'B') {
				cout << nums[1] << endl;
			} else if (order[i] == 'C') {
				cout << nums[2] << endl;
			}
		}
	}

	return 0;
}