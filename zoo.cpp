#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>
#include <algorithm>
#include <map>

using namespace std;


vector<string> split(const string &s, char delim) {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim)) {
		transform(item.begin(), item.end(), item.begin(), ::tolower);
		tokens.push_back(item);
	}
	return tokens;
}

int main() {

	int n;
	string line;

	getline(cin, line);
	n = stoi(line);
	int listIndex = 1;
	while (n != 0) {

		map<string, int> animals;

		for (int i = 0; i < n; ++i) {
			getline(cin, line);
			auto words = split(line, ' ');
			auto word = words.at(words.size() - 1);
			if (animals.find(word) != animals.end())
				animals[word]++;
			else animals[word] = 1;
		}

		cout << "List " << listIndex++ << ":" << endl;

		for (auto a : animals) {
			cout << a.first << " | " << a.second << endl;
		}

		getline(cin, line);
		n = stoi(line);
	}


	return 0;
}