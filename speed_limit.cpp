#include <iostream>
#include <string>

using namespace std;

int main() {
    int n;

    while (true) {
        cin >> n;
        if (n == -1)
            break;

        int sum = 0;
        int t0 = 0, s1, t1;

        for (int i = 0; i < n; ++i) {
            cin >> s1 >> t1;
            sum += (s1) * (t1 - t0);
            t0 = t1;
        }

        cout << sum << " miles" << endl;
    }

    return 0;
}