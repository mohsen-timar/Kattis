#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int t;
	cin >> t;

	for (int i = 0; i < t; ++i) {
		int l, n;
		cin >> l >> n;
		vector<int> ants;
		for (int j = 0; j < n; ++j) {
			int ant;
			cin >> ant;
			ants.push_back(ant);
		}

		int best = l, worse = 0;
		int mid = l / 2;
		int bestAnt;
		for (auto ant : ants) {
			if (abs(ant - mid) < best) {
				best = abs(ant - mid);
				bestAnt = ant;
			}

			int longDistance = max(abs(l - ant), ant);
			if (longDistance > worse)
				worse = longDistance;
		}

		cout << min(abs(l - bestAnt), bestAnt) << " " << worse << endl;
	}

	return 0;
}