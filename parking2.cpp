#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {
		int t;
		cin >> t;

		int sum = 0;
		vector<int> nums;

		for (int j = 0; j < t; ++j) {
			int num;
			cin >> num;
			nums.push_back(num);
		}

		sort(nums.begin(), nums.end());

		for (int k = 0; k < nums.size() - 1; ++k) {
			sum += nums.at(k + 1) - nums.at(k);
		}

		cout << sum * 2 << endl;

	}


	return 0;
}