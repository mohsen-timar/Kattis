#include <iostream>
#include <cmath>

using namespace std;

int main() {

	long long int i, j;

	while (cin >> i) {
		cin >> j;
		cout << abs(i - j) << endl;
	}

	return 0;
}