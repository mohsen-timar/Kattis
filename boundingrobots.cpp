#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int w, l;
	cin >> w >> l;
	while (w != 0 && l != 0) {
		int n;
		cin >> n;
		int x = 0, y = 0;
		int rx = 0, ry = 0;
		for (int i = 0; i < n; ++i) {
			char a;
			int m;
			cin >> a >> m;
			switch (a) {
				case 'u':
					y += m;
					ry += m;
					break;
				case 'd':
					y -= m;
					ry -= m;
					break;
				case 'l':
					x -= m;
					rx -= m;
					break;
				case 'r':
					x += m;
					rx += m;
					break;
			}

			if (x < 0) x = 0;
			if (y < 0) y = 0;
			if (y >= l) y = l - 1;
			if (x >= w) x = w - 1;
		}

		cout << "Robot thinks " << rx << " " << ry << endl;
		cout << "Actually at " << x << " " << y << endl;

		cout << endl;
		cin >> w >> l;
	}


	return 0;
}