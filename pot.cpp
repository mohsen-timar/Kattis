#include <iostream>

using namespace std;

long long power(int x, int y) {
    long long res;
    if (y == 1) {
        return x;
    }

    if (y == 0) {
        return 1;
    }

    if (x == 1)
        return 1;

    if (y % 2 == 0) {
        res = power(x, y / 2);
        return res * res;
    } else {
        res = power(x, y / 2);
        return res * res * x;
    }
}

int main() {

    int count;
    long long result = 0;

    cin >> count;

    for (int i = 0; i < count; ++i) {
        int val;
        cin >> val;
        int p = val % 10;
        val = val / 10;
        result += power(val, p);
    }

    cout << result << endl;

    return 0;
}