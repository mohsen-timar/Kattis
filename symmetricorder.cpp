#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    int n;
    cin >> n;

    int index = 0;

    while (n != 0) {

        index++;
        vector<string> names(n);
        int i = 0, j = n - 1;
        for (int k = 1; k <= n; ++k) {
            string name;
            cin >> name;
            names[i] = name;
            i++;

            if (i < j) {
                cin >> name;
                names[j] = name;
                j--;
                k++;
            }

        }

        cout << "SET " << index << endl;

        for (string name : names) {
            cout << name << endl;
        }

        cin >> n;
    }

    return 0;
}