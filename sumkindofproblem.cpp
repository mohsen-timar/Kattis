#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int p;
    cin >> p;

    for (int i = 0; i < p; ++i) {
        int k, n;
        cin >> k >> n;

        cout << k;

        int s1 = 0, s2 = 0, s3 = 0;

        for (int j = 1; j <= n; ++j) {
            s1 += j;
            s2 += 2 * j - 1;
            s3 += 2 * j;
        }

        cout << " " << s1 << " " << s2 << " " << s3 << endl;

    }

    return 0;
}