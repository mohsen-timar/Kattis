#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

	int A, B, C;
	cin >> A >> B >> C;

	int a[3], l[3], t[101]{0};

	for (int i = 0; i < 3; ++i) {
		cin >> a[i] >> l[i];
	}

	for (int j = 0; j < 3; ++j) {
		for (int i = a[j]; i < l[j]; ++i) {
			t[i]++;
		}
	}

	int cost = 0;
	for (int i = 1; i < 101; ++i) {
		if (t[i] == 1) {
			cost += A;
		} else if (t[i] == 2) {
			cost += B * 2;
		} else if (t[i] == 3) {
			cost += C * 3;
		}
	}

	cout << cost << endl;

	return 0;
}