#include <iostream>
#include <cmath>

using namespace std;

int main() {

    long b, br, bs, a, as;
    cin >> b >> br >> bs >> a >> as;

    long bm = (br - b) * bs;
    double ar = floor((bm / as) + a) + 1;

    cout << ar << endl;

    return 0;
}