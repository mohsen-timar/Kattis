#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n;
	cin >> n;

	for (int i = 1; i <= n; ++i) {
		int R, C;
		cin >> R >> C;

		char image[R][C]{0};
		for (int m = 0; m < R; ++m) {
			for (int l = 0; l < C; ++l) {
				cin >> image[R - m - 1][C - l - 1];
			}
		}

		cout << "Test " << i << endl;

		for (int m = 0; m < R; ++m) {
			for (int l = 0; l < C; ++l) {
				cout << image[m][l];
			}
			cout << endl;
		}

	}

	return 0;
}