#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int a1, b1, c1, d1, a2, b2, c2, d2;
    cin >> a1 >> b1 >> c1 >> d1;
    cin >> a2 >> b2 >> c2 >> d2;

    if (a1 + b1 + c1 + d1 == a2 + b2 + c2 + d2) {
        cout << "Tie" << endl;
    } else if (a1 + b1 + c1 + d1 > a2 + b2 + c2 + d2) {
        cout << "Gunnar" << endl;
    } else {
        cout << "Emma" << endl;
    }

    return 0;
}