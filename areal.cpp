#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

	double n;

	cin >> n;

	n = sqrt(n) * 4;

	cout << setprecision(12) << n << endl;

	return 0;
}