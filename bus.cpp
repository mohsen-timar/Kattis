#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {

		int t;
		cin >> t;
		long sum = 0;
		t--;
		sum = 1;
		for (int j = 0; j < t; ++j) {
			sum *= 2;
			sum++;
		}
		cout << sum << endl;
	}


	return 0;
}