#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

	map<char, int> penalties;
	int solvedProblem = 0;
	int timeToSolvedProblem = 0;

	while (true) {
		int n;
		char c;
		string info;

		cin >> n;
		if (n == -1)
			break;
		cin >> c >> info;
		if (info == "wrong") {
			penalties[c] += 20;
		} else {
			solvedProblem++;
			timeToSolvedProblem += n;
			if (penalties.find(c) != penalties.end()) {
				timeToSolvedProblem += penalties[c];
			}
		}
	}

	cout << solvedProblem << " " << timeToSolvedProblem << endl;


	return 0;
}