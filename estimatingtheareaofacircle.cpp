#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

	double r, m, c;
	cout << setprecision(10);
	while (true) {

		cin >> r >> m >> c;
		if (r == 0 && m == 0 && c == 0)
			break;

		cout << r * r * M_PI << " " << r * r * 4 * c / m << endl;


	}


	return 0;
}