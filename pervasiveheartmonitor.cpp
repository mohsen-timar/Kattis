#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>
#include <map>
#include <iomanip>

using namespace std;

vector<string> split(const string &s, char delim = ' ') {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}


int main() {

	string line;
	map<string, double> results;

	cout << setprecision(12);

	while (getline(cin, line)) {
		auto data = split(line);
		string name;
		double sum = 0;
		int count = 0;

		for (string i: data) {
			if (i[0] >= '0' && i[0] <= '9') {
				sum += stod(i);
				count++;
			} else {
				name += " " + i;
			}
		}

		cout << (sum / count) << name << endl;
	}

	return 0;
}