#include <iostream>
#include <cmath>

using namespace std;

unsigned long long min(unsigned long long *s, int n) {
    unsigned long long m = s[0];
    for (int i = 1; i < n; ++i) {
        if (s[i] < m) {
            m = s[i];
        }
    }

    return m;
}

int main() {

    string line;
    cin >> line;
    unsigned long long score[3]{0};
    unsigned long long sum = 0;

    for (auto ch : line) {
        switch (ch) {
            case 'T':
                score[0]++;
                break;
            case 'C':
                score[1]++;
                break;
            case 'G':
                score[2]++;
                break;
        }
    }

    if (score[0] > 0 && score[1] > 0 && score[2] > 0) {
        sum += 7 * min(score, 3);
    }

    sum += score[0] * score[0] + score[1] * score[1] + score[2] * score[2];

    cout << sum << endl;

    return 0;
}