#include <iostream>
#include <cmath>

using namespace std;

string ab;
string nums;

unsigned long long toTen(string number, int base) {
	unsigned long long result = 0;

	for (int i = 0; i < number.size(); ++i) {
		result = result * base + (nums.find(number[i]));
	}

	return result;
}

unsigned long long calc(unsigned long long n1, unsigned long long n2, char op) {
	switch (op) {
		case '+':
			return n1 + n2;
		case '-':
			return n1 - n2;
		case '*':
			return n1 * n2;
		case '/':
			double k = (double) n1 / (double) n2;
			if ((int) k != k)
				return static_cast<unsigned long long int>(-1);
			return static_cast<unsigned long long int>(k);
	}
}

unsigned long findMinimumBase(string n1, string n2, string n3) {
	unsigned long max = 0;
	unsigned long k;
	bool unary = true;
	for (int i = 0; i < n1.length(); ++i) {
		k = nums.find(n1[i]);
		if (k != 1) {
			unary = false;
		}
		if (k > max)
			max = k;
	}

	for (int i = 0; i < n2.length(); ++i) {
		k = nums.find(n2[i]);
		if (k != 1) {
			unary = false;
		}
		if (k > max)
			max = k;
	}

	for (int i = 0; i < n3.length(); ++i) {
		k = nums.find(n3[i]);
		if (k != 1) {
			unary = false;
		}
		if (k > max)
			max = k;
	}

	if (unary) {
		return 1;
	}

	return max + 1;
}

int main() {

	nums = "0";

	for (int i = 1; i <= 9; ++i) {
		ab += to_string(i);
		nums += to_string(i);
	}

	for (char j = 'a'; j <= 'z'; ++j) {
		ab += j;
		nums += j;
	}

	ab += '0';

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {

		string ns1, op, ns2, e, ns3;

		cin >> ns1 >> op >> ns2 >> e >> ns3;
		int count = 0;
		auto minBase = findMinimumBase(ns1, ns2, ns3);
		for (auto j = minBase; j <= ab.length(); ++j) {
			auto n1 = toTen(ns1, j), n2 = toTen(ns2, j), n3 = toTen(ns3, j);
			auto res = calc(n1, n2, op[0]);
			if (res == n3) {
				count++;
				cout << ab[j - 1];
			}
//			else {
//				cout << n1 << op << n2 << "=" << n3 << "!=" << res << endl;
//			}
		}

		if (count == 0) {
			cout << "invalid";
		}

		cout << endl;
	}

	return 0;
}