#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n;
    cin >> n;

    int a = 1;
    int b = 0;

    for (int i = 0; i < n; ++i) {
        int tmp = a + b;
        a = b;
        b = tmp;
    }

    cout << a << " " << b << endl;

    return 0;
}