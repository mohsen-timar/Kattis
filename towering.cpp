#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int comp(const void *e1, const void *e2) {
	int i = *(int *) e1, j = *(int *) e2;
	if (i > j)
		return 1;
	if (j > i)
		return -1;
	return 0;
}

int main() {

	int boxes[6];
	int big_boxes[3];
	int small_boxes[3];

	int h;

	for (int i = 0; i < 6; ++i) {
		cin >> boxes[i];
	}

	int h1, h2;
	cin >> h1 >> h2;

	qsort(boxes, sizeof(boxes) / sizeof(*boxes), sizeof(*boxes), comp);

	h1 -= boxes[5];
	big_boxes[0] = 5;
	bool flag = false;
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			if (boxes[i] + boxes[j] == h1) {
				big_boxes[1] = i;
				big_boxes[2] = j;
				flag = true;
				break;
			}
		}
		if (flag)
			break;
	}

	for (int i = 0, k = 0; i < 6; ++i) {
		flag = false;
		for (int j = 0; j < 3; ++j) {
			if (big_boxes[j] == i) {
				flag = true;
				break;
			}
		}
		if (flag)
			continue;
		small_boxes[k] = i;
		k++;
	}

	for (int i = 0; i < 3; ++i) {
		big_boxes[i] = boxes[big_boxes[i]];
		small_boxes[i] = boxes[small_boxes[i]];
	}

	qsort(big_boxes, sizeof(big_boxes) / sizeof(*big_boxes), sizeof(*big_boxes), comp);
	qsort(small_boxes, sizeof(small_boxes) / sizeof(*small_boxes), sizeof(*small_boxes), comp);

	for (int i = 2; i >= 0; --i) {
		cout << big_boxes[i] << " ";
	}

	for (int i = 2; i >= 0; --i) {
		cout << small_boxes[i];
		if (i == 0) {
			cout << endl;
		} else {
			cout << " ";
		}
	}

	return 0;
}