#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int x, y, n;

    cin >> x >> y >> n;

    for (int i = 1; i <= n; ++i) {
        bool flag = false;
        if (i % x == 0) {
            cout << "Fizz";
            flag = true;
        }
        if (i % y == 0) {
            cout << "Buzz";
            flag = true;
        }

        if (!flag) {
            cout << i;
        }

        cout << endl;
    }


    return 0;
}