#include <iostream>
#include <cmath>

using namespace std;

int calc(int n) {
    int sum = 0;
    while (n != 0) {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}

int main() {

    int l, d, x;

    cin >> l >> d >> x;

    for (int i = l; i <= d; ++i) {
        if (calc(i) == x) {
            cout << i << endl;
            break;
        }
    }

    for (int i = d; i >= l; --i) {
        if (calc(i) == x) {
            cout << i << endl;
            break;
        }
    }

    return 0;
}