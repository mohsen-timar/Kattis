#include <iostream>
#include <cmath>

using namespace std;

int isVowel(char ch) {
    return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
}

int main() {

    string n;
    getline(cin, n);

    string res;

    for (int i = 0; i < n.length(); ++i) {
        char ch = n[i];
        if (isVowel(ch)) {
            i += 2;
        }
        res += ch;
    }

    cout << res << endl;

    return 0;
}