#include <iostream>
#include <string>

using namespace std;

void swapZero(int board[][4], int command, int index) {

	if (command == 0) {

		for (int i = 0, j = 0; i < 4; ++i) {
			if (board[index][i] != 0) {
				if (j != i) {
					board[index][j] = board[index][i];
					board[index][i] = 0;
				}
				j++;
			}
		}

	} else if (command == 1) {

		for (int i = 0, j = 0; i < 4; ++i) {
			if (board[i][index] != 0) {
				if (j != i) {
					board[j][index] = board[i][index];
					board[i][index] = 0;
				}
				j++;
			}
		}

	} else if (command == 2) {

		for (int i = 3, j = 3; i >= 0; --i) {
			if (board[index][i] != 0) {
				if (j != i) {
					board[index][j] = board[index][i];
					board[index][i] = 0;
				}
				j--;
			}
		}
	} else if (command == 3) {

		for (int i = 3, j = 3; i >= 0; --i) {
			if (board[i][index] != 0) {
				if (j != i) {
					board[j][index] = board[i][index];
					board[i][index] = 0;
				}
				j--;
			}
		}

	}

}

int main() {

	int board[4][4];

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			cin >> board[i][j];
		}
	}

	int command;
	cin >> command;

	if (command == 0) {

		for (int i = 0; i < 4; ++i) {
			swapZero(board, command, i);
			for (int j = 0; j < 3; ++j) {
				if (board[i][j] == board[i][j + 1]) {
					board[i][j] *= 2;
					board[i][j + 1] = 0;
					swapZero(board, command, i);
				}
			}
		}

	} else if (command == 1) {

		for (int i = 0; i < 4; ++i) {
			swapZero(board, command, i);
			for (int j = 0; j < 3; ++j) {
				if (board[j][i] == board[j + 1][i]) {
					board[j][i] *= 2;
					board[j + 1][i] = 0;
					swapZero(board, command, i);
				}
			}
		}

	} else if (command == 2) {

		for (int i = 0; i < 4; ++i) {
			swapZero(board, command, i);
			for (int j = 3; j > 0; --j) {
				if (board[i][j] == board[i][j - 1]) {
					board[i][j] *= 2;
					board[i][j - 1] = 0;
					swapZero(board, command, i);
				}
			}
		}

	} else if (command == 3) {

		for (int i = 0; i < 4; ++i) {
			swapZero(board, command, i);
			for (int j = 3; j > 0; --j) {
				if (board[j][i] == board[j - 1][i]) {
					board[j][i] *= 2;
					board[j - 1][i] = 0;
					swapZero(board, command, i);
				}
			}
		}

	}

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			cout << board[i][j];

			if (j < 3) {
				cout << " ";
			} else {
				cout << endl;
			}
		}
	}

	return 0;
}