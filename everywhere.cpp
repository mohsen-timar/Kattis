#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

    int a;
    cin >> a;

    for (int i = 0; i < a; ++i) {
        int b;
        cin >> b;
        vector<string> strings;
        int c = 0;
        for (int j = 0; j < b; ++j) {
            string str;
            getline(cin, str);
            if (str.empty()) {
                j--;
                continue;
            }
            bool flag = false;
            for (string k:strings) {
                if (k == str) {
                    flag = true;
                    break;
                }
            }
            if (flag)
                continue;
            strings.push_back(str);
            c++;
        }

        cout << c << endl;
    }

    return 0;
}