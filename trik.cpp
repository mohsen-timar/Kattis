#include <iostream>
#include <string>

using namespace std;

void swap(bool &a, bool &b) {
    bool c = a;
    a = b;
    b = c;
}

int main() {

    string line;
    getline(cin, line);

    bool cups[3]{false};
    cups[0] = true;

    for (int i = 0; i < line.length(); ++i) {
        if (line[i] == 'A') {
            swap(cups[0], cups[1]);
        } else if (line[i] == 'B') {
            swap(cups[1], cups[2]);
        } else if (line[i] == 'C') {
            swap(cups[0], cups[2]);
        }
    }

    for (int i = 0; i < 3; ++i) {
        if (cups[i]) {
            cout << (i+1) << endl;
            return 0;
        }
    }

    return 0;
}