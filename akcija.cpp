#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;
	vector<int> prices;
	for (int i = 0; i < n; ++i) {
		int k;
		cin >> k;
		prices.push_back(k);
	}

	std::sort(prices.begin(), prices.end());

	unsigned long freePriceGroup = prices.size() / 3;
	unsigned long startAt = prices.size() % 3;

	int priceSum = 0;

	for (unsigned long i = 0; i < freePriceGroup; ++i) {
		priceSum += prices.at(startAt + i * 3 + 1) + prices.at(startAt + i * 3 + 2);
	}
	for (unsigned long i = 0; i < startAt; ++i) {
		priceSum += prices.at(i);
	}

	cout << priceSum << endl;

	return 0;
}