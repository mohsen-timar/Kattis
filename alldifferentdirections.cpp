#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>

using namespace std;

vector<string> split(const string &s, char delim) {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}

int main() {

	int n;
	cin >> n;
	while (n != 0) {
		string line;
		getline(cin, line);

		double xfinal[n], yfinal[n];
		for (int i = 0; i < n; ++i) {
			getline(cin, line);
			auto tokens = split(line, ' ');
			double startX = stod(tokens.at(0)), startY = stod(tokens.at(1));
			double angle = stod(tokens.at(3));

			for (int j = 4; j < tokens.size(); ++j) {
				if (tokens.at(j) == "turn") {
					j++;
					angle += stod(tokens.at(j));
				} else {
					double s, c;
					sincos(angle * M_PI / 180.0, &s, &c);
					j++;
					startX += stod(tokens.at(j)) * c;
					startY += stod(tokens.at(j)) * s;
				}
			}

			xfinal[i] = startX;
			yfinal[i] = startY;
		}

		double avgX = 0, avgY = 0;
		for (int k = 0; k < n; ++k) {
			avgX += xfinal[k];
			avgY += yfinal[k];
		}

		avgX = avgX / n;
		avgY = avgY / n;

		double worseOff = 0;
		for (int k = 0; k < n; ++k) {
			double off = sqrt(pow(avgX - xfinal[k], 2) + pow(avgY - yfinal[k], 2));
			if (off > worseOff)
				worseOff = off;
		}

		cout << avgX << " " << avgY << " " << worseOff << endl;

		cin >> n;
	}


	return 0;
}