#include <iostream>
#include <string>

using namespace std;

int main() {

    int x[42]{0};

    for (int i = 0; i < 10; ++i) {
        int n;
        cin >> n;
        x[n % 42]++;
    }

    int c = 0;

    for (int i = 0; i < 42; ++i) {
        if (x[i] != 0)
            c++;
    }

    cout << c << endl;

    return 0;
}