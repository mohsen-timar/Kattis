#include <iostream>
#include <cmath>
#include <vector>
#include <limits>

using namespace std;

int main() {

	int n;
	int c = 1;
	while (cin >> n) {
		long long max = std::numeric_limits<long long>::min(), min = std::numeric_limits<long long>::max();
		for (int i = 0; i < n; ++i) {
			long long k;
			cin >> k;
			if (k > max) max = k;
			if (k < min) min = k;
		}

		cout << "Case " << c << ": " << min << " " << max << " " << (max - min) << endl;
		c++;
	}


	return 0;
}