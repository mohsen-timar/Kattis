#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {
		int k, p, q;
		char e;

		cin >> k >> p >> e >> q;

		string s;

		while (!(p == 1 && q == 1)) {

			if (p > q) {
				p -= q;
				s.push_back('1');
			} else {
				q -= p;
				s.push_back('0');
			}

		}

		s.push_back('1');

		int val = 0;

		for (int i = s.size() - 1; i >= 0; i--) {
			val = val * 2 + (s.at(i) - '0');
		}

		cout << k << " " << val << endl;
	}

	return 0;
}