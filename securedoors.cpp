#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

	int n;
	cin >> n;

	map<string, bool> users;

	for (int i = 0; i < n; ++i) {
		string state, name;
		cin >> state >> name;
		auto user = users.find(name);
		if (user == users.end()) {
			users[name] = !(state == "exit");
			if (!users[name]) {
				cout << name << " exited (ANOMALY)" << endl;
			} else {
				cout << name << " entered" << endl;
			}
		} else {
			bool sb = !(state == "exit");
			if (sb) {
				if (users[name]) {
					cout << name << " entered (ANOMALY)" << endl;
				} else {
					cout << name << " entered" << endl;
				}
			} else {
				if (!users[name]) {
					cout << name << " exited (ANOMALY)" << endl;
				} else {
					cout << name << " exited" << endl;
				}
			}
			users[name] = sb;
		}
	}


	return 0;
}