#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int R, C, Zr, Zc;
    cin >> R >> C >> Zr >> Zc;

    char ch[R][C];

    for (int i = 0; i < R; ++i) {
        for (int j = 0; j < C; ++j) {
            cin >> ch[i][j];
        }
    }

    for (int i = 0; i < R; ++i) {
        for (int j = 0; j < Zr; ++j) {
            for (int l = 0; l < C; ++l) {
                for (int k = 0; k < Zc; ++k) {
                    cout << (char)ch[i][l];
                }
            }
            cout << endl;
        }
    }


    return 0;
}