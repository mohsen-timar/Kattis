#include <iostream>

using namespace std;

int main() {

    int count;

    cin >> count;

    int result = 0;

    for (int i = 0; i < count; ++i) {
        int x;
        cin >> x;
        if (x < 0) {
            result++;
        }
    }

    cout << result << endl;

    return 0;
}