#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int R, C;
    cin >> R >> C;

    char m[R][C];

    for (int i = 0; i < R; ++i) {
        for (int j = 0; j < C; ++j) {
            cin >> m[i][j];
        }
    }

    int l0 = 0, l1 = 0, l2 = 0, l3 = 0, l4 = 0;

    for (int i = 0; i < R - 1; ++i) {
        for (int j = 0; j < C - 1; ++j) {

            int d = 0, x = 0, s = 0;

            for (int k = 0; k < 2; ++k) {
                for (int l = 0; l < 2; ++l) {
                    if (m[i + k][j + l] == '.')
                        d++;
                    else if (m[i + k][j + l] == 'X') x++;
                }
            }

            if (d == 4)
                l0++;
            else if (d == 3 && x == 1)
                l1++;
            else if (d == 2 && x == 2)
                l2++;
            else if (d == 1 && x == 3)
                l3++;
            else if (d == 0 && x == 4)
                l4++;
        }
    }

    cout << l0 << endl << l1 << endl << l2 << endl << l3 << endl << l4 << endl;

    return 0;
}