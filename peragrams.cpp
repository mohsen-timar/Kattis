#include <iostream>
#include <cmath>

using namespace std;

int main() {

	string line;
	cin >> line;

	int cc[26]{0};

	for (int i = 0; i < line.length(); ++i) {
		cc[line[i] - 'a']++;
	}

	int removed = 0;

	for (int i = 0; i < 26; ++i) {
		if (cc[i] % 2 != 0) {
			removed++;
		}
	}

	if (removed == 0) {
		cout << removed << endl;
		return 0;
	}

	if ((line.length() - removed) % 2 == 0)
		removed--;

	cout << removed << endl;

	return 0;
}