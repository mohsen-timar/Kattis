#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

    vector<int> numbers;

    for (int i = 0; i < 3; ++i) {
        int k;
        cin >> k;
        numbers.push_back(k);
    }

    cout << (max({numbers[1] - numbers[0], numbers[2] - numbers[1]}) - 1) << endl;
    return 0;
}